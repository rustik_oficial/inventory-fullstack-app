package uz.rustam.inventorybackendapp.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.rustam.inventorybackendapp.entity.enums.Roles;
import uz.rustam.inventorybackendapp.entity.template.AbsIntEntity;

import javax.persistence.*;


@Entity
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "roles")
public class Role extends AbsIntEntity {

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Roles name;

    public Role(Roles name) {
        this.name = name;
    }
}
