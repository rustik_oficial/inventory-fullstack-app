package uz.rustam.inventorybackendapp.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.rustam.inventorybackendapp.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "username"),
        @UniqueConstraint(columnNames = "email")
})

public class User extends AbsEntity {

    @Column(nullable = false, length = 20)
    String username;

    @Column(nullable = false, length = 50)
    String email;

    @Column(nullable = false, length = 120)
    String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @ToString.Exclude
    Set<Role> roles;

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

}
