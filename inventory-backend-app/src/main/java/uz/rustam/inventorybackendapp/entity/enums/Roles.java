package uz.rustam.inventorybackendapp.entity.enums;

public enum Roles {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
