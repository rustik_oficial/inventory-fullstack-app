package uz.rustam.inventorybackendapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.rustam.inventorybackendapp.entity.Role;
import uz.rustam.inventorybackendapp.entity.enums.Roles;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(Roles name);
}
