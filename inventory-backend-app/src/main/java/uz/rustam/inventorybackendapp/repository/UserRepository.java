package uz.rustam.inventorybackendapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.rustam.inventorybackendapp.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
