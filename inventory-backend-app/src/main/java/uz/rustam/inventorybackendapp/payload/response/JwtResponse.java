package uz.rustam.inventorybackendapp.payload.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;
import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtResponse {
    String accessToken;
    String type = "Bearer";
    UUID id;
    String username;
    String email;
    Collection<String> roles;

    public JwtResponse(String accessToken, UUID id, String username, String email, Collection<String> roles) {
        this.accessToken = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }
}
