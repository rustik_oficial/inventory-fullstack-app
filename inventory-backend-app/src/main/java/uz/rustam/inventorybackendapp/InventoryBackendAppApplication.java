package uz.rustam.inventorybackendapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventoryBackendAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryBackendAppApplication.class, args);
    }

}
